/* Copyright (C) 2005-2012, 2020-2022 Enrico Rossi
 * LGPL 3

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
 */

#ifndef _ANEMOMETER_H_
#define _ANEMOMETER_H_

#include <complex>
#include <vector>

using namespace std;

class Anemometer {
  public:
    enum direction_t {
      NORTH,
      NORTH_EAST,
      EAST,
      SOUTH_EAST,
      SOUTH,
      SOUTH_WEST,
      WEST,
      NORTH_WEST
    };

    enum tendency_t {
      INCREASE,
      DECREASE,
      STABLE
    };

    vector<complex<double>> samples;

    Anemometer(unsigned = 60 * 15); // default 15 minutes x 1 sample/sec.
    void add(double, unsigned);
    double min();
    double max();
    complex<double> average();
    void average(double*, double*);
    void average(double*, unsigned*);
    direction_t direction(unsigned);
};

#endif
