/* Copyright (C) 2005-2012, 2020-2022 Enrico Rossi
 * LGPL 3

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

/*! \file
 *
 */

#include <algorithm>
#include <complex>
#include <numeric>
#include <vector>
#include "anemometer.h"

using namespace std;

/**
 */
Anemometer::Anemometer(unsigned s) : samples(s, complex<double>(0, 0))
{
}

/** push a new value into the array.
 *
 * Convert a new value into a complex number and
 * push it at the end of the vector.
 *
 * @param speed wind speed
 * @param degree 0° - 360°
 *
 */
void Anemometer::add(double speed, unsigned degree)
{
  const double PI { 3.14159265358979323846 };

  // rotate the wind array 1 element left
  rotate(samples.begin(), samples.begin() + 1, samples.end());

  // convert wind direction from degrees to radiant
  // radiant = degrees * 2PI / 360
  double theta = (double)degree * (PI / 180);

  /* convert from polar p(r,theta) to complex p(x,y)
   * and add it at the end of the vector.
   */
  samples.back() = polar(speed, theta);
}

/** Find the minimum in the vector.
 *
 * Iterate through the vector and check the modulus
 * of each element (vector rho lenght) for the
 * minimum value.
 */
double Anemometer::min()
{
  double m { abs(*samples.begin()) }; // min = 1st element

  for (auto iter : samples)
    if (abs(iter) < m)
      m = abs(iter);
 
  return m;
}

/** Find the maximum in the vector.
 *
 * @see Anemometer::min()
 */
double Anemometer::max()
{
  double M { abs(*samples.begin()) }; // 1st element

  for (auto iter : samples)
    if (abs(iter) > M)
      M = abs(iter);
 
  return M;
}

/** Find the average in the vector as complex.
 *
 * @return the average of the elements in the
 * vector as a complex number.
 */
complex<double> Anemometer::average()
{
  complex<double> sum = accumulate(
      samples.begin(),
      samples.end(),
      complex<double>(0, 0)
      );

  return complex<double>(sum.real() / samples.size(), sum.imag() / samples.size());
}

/** Find the average in the vector as rho, theta.
 *
 */
void Anemometer::average(double* rho, double* theta)
{
  *rho = abs(average());
  // Convert theta (radiant) to 360 degrees atan (-PI to PI)
  *theta = arg(average()); // theta angle in radiants
}

/** Find the average in the vector as rho, degrees.
 *
 */
void Anemometer::average(double* rho, unsigned* degrees)
{
  const double PI { 3.14159265358979323846 };
  double theta;

  average(rho, &theta);

  // Convert theta (radiant) to 360 degrees (-PI to PI)
  *degrees = abs(theta * 180 / PI);

  if (theta < 0)
    *degrees = 360 - *degrees;
}

/** Get the wind direction.
 *
 * @param the degrees 0 - 360
 * @return the direction.
 */
Anemometer::direction_t Anemometer::direction(unsigned degrees)
{
  if ((degrees >= 23) && (degrees < 68))
    return NORTH_EAST;
  else if ((degrees >= 68) && (degrees < 113))
    return EAST;
  else if ((degrees >= 113) && (degrees < 158))
    return SOUTH_EAST;
  else if ((degrees >= 158) && (degrees < 203))
    return SOUTH;
  else if ((degrees >= 203) && (degrees < 248))
    return SOUTH_WEST;
  else if ((degrees >= 248) && (degrees < 293))
    return WEST;
  else if ((degrees >= 293) && (degrees < 338))
    return NORTH_WEST;
  else
    return NORTH;
}
