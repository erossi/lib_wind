/* Copyright (C) 2022 Enrico Rossi
 * LGPL 3

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this library; If not, see <http://www.gnu.org/licenses/>.
*/

/** \file
 *
 */

#include <complex>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <vector>
#include <time.h>
#include "anemometer.h"

void doit(class Anemometer* w) {
  double speed;
  unsigned angle;
  complex<double> S { 0,0 }; // Sum of real and imaginary

  cout << "List complex elements and it's progressive sum." << endl;

  for (auto iter : w->samples) {
    S += iter;
    cout << "v= " << iter << " ∑= " << S << endl;
  }

  complex<double> sum = std::accumulate(
      w->samples.begin(),
      w->samples.end(),
      complex<double>(0, 0)
      );

  cout << "Accumulate(): " << sum << endl;
  cout << "Min: " << w->min() << endl;
  cout << "Max: " << w->max() << endl;

  complex<double> c = w->average();
  double rho, theta;
  w->average(&rho, &theta);
  w->average(&speed, &angle);

  cout << "Avg<complex>: " << c << endl;
  cout << "Avg (r, t): " << rho << ", " << theta << endl;
  cout << "Avg: " << speed << ", " << angle << "°" << endl;
  cout << "Dir: " << w->direction(angle) << endl << endl;
}

int main()
{
  const unsigned s { 5 };
  Anemometer w(s);
  double speed;
  unsigned angle;

  speed = 10;

  angle = 0;
  cout << "add: " << speed << ", " << angle << "°" << endl;
  w.add(speed, angle);
  angle = 90;
  cout << "add: " << speed << ", " << angle << "°" << endl;
  w.add(speed, angle);
  angle = 180;
  cout << "add: " << speed << ", " << angle << "°" << endl;
  w.add(speed, angle);
  angle = 270;
  cout << "add: " << speed << ", " << angle << "°" << endl;
  w.add(speed, angle);
  angle = 270;
  cout << "add: " << speed << ", " << angle << "°" << endl;
  w.add(speed, angle);

  cout << endl;
  doit(&w);

  cout << "Using random generated vector." << endl;

  // srand (time(NULL)); // initialize the seed
  srand(1);

  for (unsigned i=0; i < s; i++) {
    speed = (double)(rand() % 1000) / 10;
    angle = rand() % 360;

    cout << "Add: " << speed << ", " << angle << "°" << endl;
    w.add(speed, angle);
  }

  cout << endl;
  doit(&w);

  return 0;
}
